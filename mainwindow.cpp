#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QWebEngineView>
#include <QWebEngineSettings>
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QWidget *wgt = new QWidget(this);
    QGridLayout *gridLayout = new QGridLayout(wgt);

    wgt->setStyleSheet(QString::fromUtf8("border: 1px solid black;\n"
                                          "border-radius: 25px;background-color:black;"));
    ui->centralwidget->layout()->addWidget(wgt);


    QWebEngineView* view = new QWebEngineView(wgt);
    view->setWindowTitle("Qt 6 - The Ultimate UX Development Platform");

    view->setUrl(QUrl("https://www.youtube.com/embed/TodEc77i4t4"));

    view->settings()->setAttribute(QWebEngineSettings::PluginsEnabled, true);
    view->settings()->setAttribute(QWebEngineSettings::FullScreenSupportEnabled, true);
    view->settings()->setAttribute(QWebEngineSettings::AllowRunningInsecureContent, true);
    view->settings()->setAttribute(QWebEngineSettings::SpatialNavigationEnabled, true);
    view->settings()->setAttribute(QWebEngineSettings::JavascriptEnabled, true);
    view->settings()->setAttribute(QWebEngineSettings::JavascriptCanOpenWindows, true);

    wgt->layout()->addWidget(view);

}

MainWindow::~MainWindow()
{
    delete ui;
}

